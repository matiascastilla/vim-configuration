" All system-wide defaults are set in $VIMRUNTIME/debian.vim (usually just
" /usr/share/vim/vimcurrent/debian.vim) and sourced by the call to :runtime
" you can find below.  If you wish to change any of those settings, you should
" do it in this file (/etc/vim/vimrc), since debian.vim will be overwritten
" everytime an upgrade of the vim packages is performed.  It is recommended to
" make changes after sourcing debian.vim since it alters the value of the
" 'compatible' option.

" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages available in Debian.
runtime! debian.vim

if has('python3')
elseif has('python')
endif

" Uncomment the next line to make Vim more Vi-compatible
" NOTE: debian.vim sets 'nocompatible'.  Setting 'compatible' changes numerous
" options, so any other options should be set AFTER setting 'compatible'.
"set compatible

" Vim5 and later versions support syntax highlighting. Uncommenting the next
" line enables syntax highlighting by default.
"syntax on

" If using a dark background within the editing area and syntax highlighting
" turn on this option as well
"set background=dark

" Uncomment the following to have Vim jump to the last position when
" reopening a file
"if has("autocmd")
"  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
"endif

" Uncomment the following to have Vim load indentation rules and plugins
" according to the detected filetype.
"if has("autocmd")
"  filetype plugin indent on
"endif

" The following are commented out as they cause vim to behave a lot
" differently from regular Vi. They are highly recommended though.
"set showcmd		" Show (partial) command in status line.
"set showmatch		" Show matching brackets.
"set ignorecase		" Do case insensitive matching
"set smartcase		" Do smart case matching
"set incsearch		" Incremental search
"set autowrite		" Automatically save before commands like :next and :make
"set hidden             " Hide buffers when they are abandoned
"set mouse=a		" Enable mouse usage (all modes)
"
"

func! WordProcessorMode() 
  setlocal formatoptions=1 
  setlocal noexpandtab 
  map j gj 
  map k gk
  set formatprg=par
  setlocal wrap 
  setlocal linebreak
  match OverLength // 
  colorscheme Mustang
"  set background=light
  call acp#disable()
  set nonu
endfu 
com! PP call WordProcessorMode()


"set background=dark
"colorscheme solarized 

colorscheme simple-dark

" Source a global configuration file if available
if filereadable("/etc/vim/vimrc.local")
  source /etc/vim/vimrc.local
endif

set noswapfile
set expandtab
set tabstop=4
set shiftwidth=4
set nobackup
set nowritebackup
set number

highlight OverLength ctermbg=red ctermfg=white guibg=#592929
match OverLength /\%81v.\+/

filetype plugin indent on


filetype plugin on
syntax on

set path=$PWD/**

au FileType python setlocal tabstop=8 shiftwidth=4 softtabstop=4 smarttab expandtab
au FileType php setlocal tabstop=4 expandtab smarttab shiftwidth=4

highlight OverLength ctermbg=red ctermfg=white guibg=#592929
match OverLength /\%81v.\+/

let g:pathogen_disabled = []

if !exists("g:ruby")
    call add(g:pathogen_disabled, 'vim-ruby-refactoring')
    call add(g:pathogen_disabled, 'rubocop')
else    
    au FileType ruby map <buffer> <F7> :RuboCop<CR>
endif    

if !exists("g:python")
    call add(g:pathogen_disabled, 'ropevim')
    call add(g:pathogen_disabled, 'python-mode')
    let ropevim_extended_complete=1
else
    cnoremap python py3<Space>
    " Pathogen load
    filetype off

    filetype plugin indent on
    syntax on
    let g:pymode_python = 'python3'
    let g:pymode_rope = 1
    let g:pymode_rope_autoimport = 1
endif    

if !exists("g:ocaml")
    call add(g:pathogen_disabled, 'sensible')
else    
    echo "OCaml detected"
    let g:opamshare = substitute(system('opam config var share'),'\n$','','''')
    execute "set rtp+=" . g:opamshare . "/merlin/vim"
endif    

if !exists("g:golang")
    call add(g:pathogen_disabled, 'vim-go')
    call add(g:pathogen_disabled, 'godoctor.vim')
endif    



call pathogen#infect()
call pathogen#helptags()


set wildignore=*.pyc
set wildignore+=__pycache__

set guifont=monospace\ 11

map <C-F4> :q<enter>
map <`> :tabfirst<enter>

if filereadable("~/.vim/plugin/autotag.vim")
    source ~/.vim/plugin/autotag.vim
endif

hi TabLine ctermfg=Black ctermbg=Green
hi TabLineFill ctermfg=Black ctermbg=DarkGreen
autocmd FileType rust colorscheme Mustang
set hls


autocmd FileType javascript setlocal shiftwidth=2 softtabstop=2 expandtab

"map <up> <nop>"
"map <down> <nop>"
"map <left> <nop>"
"map <right> <nop>"
"imap <up> <nop>"
"imap <down> <nop>"
"imap <left> <nop>"
"imap <right> <nop>"
"set exrc"
